const https = require('https')
const options = {
    hostname: 'my.zerotier.com',
    port: 443,
    path: '/api/network/<network id here>',
    method: 'GET',
    headers: {
        'Authorization': 'bearer <token here>'
    }
}

const req = https.request(options, res => {
    console.log(`statusCode: ${res.statusCode}`)
    let body = '';

    res.on('data', d => {
        body += d.toString();
        // process.stdout.write(d)
    }).on('end', () => {                    
        const schema = JSON.parse(body);
        // process.stdout.write(schema)
        console.log(schema)
    })
})

req.on('error', error => {
    console.error(error)
})

req.end()