
const https = require('https')

module.exports = function(RED) {
    function GetStatusNode(config) {
        RED.nodes.createNode(this,config);

        // Retrieve the config node
        this.server = RED.nodes.getNode(config.server);

        var node = this;
        
        node.on('input', function(msg) {
            const options = {
                hostname: this.server.domain,
                port: 443,
                path: '/api/network/93afae5963db32f3',
                method: 'GET',
                headers: {
                    'Authorization': 'bearer ' + this.server.apiKey
                }
            }
            
            const req = https.request(options, res => {
                let body = '';
                
                res.on('data', d => {
                    body += d.toString();
                }).on('end', () => {                    
                    if (res.statusCode == 200) {
                        node.status({
                            text: `statusCode: ${res.statusCode}`,
                            fill: 'green'
                        })

                        const schema = JSON.parse(body);
                        msg.payload = schema;
                        node.send(msg);
                    } else {
                        node.send(body)
                        console.error(body)
                        node.status({
                            text: `statusCode: ${res.statusCode}`,
                            fill: 'red'
                        })
                    }
                })
            })
            
            req.on('error', error => {
                node.send(error)
                console.error(error)
                node.status({
                    text: `statusCode: ${res.statusCode}`,
                    fill: 'red'
                })
            })
            
            req.end()
        })
    }

    RED.nodes.registerType("get-status", GetStatusNode);
}


