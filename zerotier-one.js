module.exports = function(RED) {
    function ZeroTierOneNode(n) {
        RED.nodes.createNode(this, n);

        if (n.domain.indexOf('://') >= 0) {
            this.domain = n.domain.substr(n.domain.indexOf('://') +3, n.domain.length)
        } else {
            this.domain = n.domain;
        }

        this.apiKey = n.apiKey;
    }

    RED.nodes.registerType("zerotier-one", ZeroTierOneNode);
}